Concrete Specialist provides professional concrete repair services in Summit County, Eagle County, and surrounding areas. We use the latest foam injection technology to repair sinking and de-stabilized slabs. We also seal and resurface damaged concrete. All our work comes with a one year guarantee.

Address: 800 Copper Rd, #3747, Frisco, CO 80443, USA || Phone: 970-409-4141
